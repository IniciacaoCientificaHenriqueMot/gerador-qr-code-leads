import React from 'react'
import { Card } from 'react-bootstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faHome } from '@fortawesome/free-solid-svg-icons'

export default function Barra(props) {
	const { buttons } = props || []
	const home = buttons.find(e => e.name === 'home')
	return (
		<>
			<Card.Header>
				{buttons && home && (
					<a className='float-right btn btn-secondary' href={home.url} title='Home'>
						<FontAwesomeIcon icon={faHome} />
					</a>
				)}
			</Card.Header>
		</>
	)
}
