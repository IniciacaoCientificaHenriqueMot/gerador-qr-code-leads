import React from 'react'
import Loader from 'react-loader-spinner'

export default function Loading() {
	return (
		<center>
			<Loader type='ThreeDots' color='#00BFFF' height={30} width={30} />
		</center>
	)
}
