import Barra from './barra'
import Loading from './loading'
import ModalQRCode from './modal-qrcode'
import QRCode from './qrcode'

export { Barra, Loading, ModalQRCode, QRCode }
