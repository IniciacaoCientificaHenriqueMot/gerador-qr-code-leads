import React, { useRef } from 'react'
import { Modal } from 'react-bootstrap'
import { QRCode } from '../components'
import ReactToPrint from 'react-to-print'

export default function ModalQRCode(props) {
	const componentRef = useRef()
	const { innerHeight } = window

	return (
		<>
			<Modal size='lg' show={props.show} onHide={props.onHide} aria-labelledby='example-modal-sizes-title-lg'>
				{/* <Modal.Header closeButton>
					<Modal.Title>QRCode Generator Estácio</Modal.Title>
				</Modal.Header> */}
				<Modal.Header>
					<ReactToPrint
						trigger={() => <button className='btn btn-secondary'>imprimir</button>}
						content={() => componentRef.current}
						className='float-right'
					/>
				</Modal.Header>
				<Modal.Body style={{ height: '100%' }}>
					<div ref={componentRef}>
						<QRCode data={props.data} />
					</div>
				</Modal.Body>
			</Modal>
		</>
	)
}
