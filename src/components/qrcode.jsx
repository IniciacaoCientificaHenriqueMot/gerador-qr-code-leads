import React from 'react'
import { Container, Row, Col } from 'react-bootstrap'
import { QRCode as Code } from 'react-qr-svg'

export default function QRCode(props) {
	return (
		<>
			<Container>
				<Row>
					<Col>
						<center>
							{props.data.title && <div className='qrcode-bar'>{props.data.title}</div>}
							<Code value={props.data.url || ''} className='qrcode-img' />
						</center>
						<div className='qrcode-bar'>{props.data.descricao || ''}</div>
					</Col>
				</Row>
			</Container>
		</>
	)
}
