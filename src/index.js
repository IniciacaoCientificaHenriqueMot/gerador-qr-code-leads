import React from 'react'
import ReactDOM from 'react-dom'
import App from './routes/index'
import * as serviceWorker from './serviceWorker'

import './theme/index.css'
import 'bootstrap/dist/css/bootstrap.min.css'

/** react alert */
import { transitions, positions, Provider as AlertProvider } from 'react-alert'
import AlertTemplate from 'react-alert-template-basic'

// optional cofiguration
const options = {
	position: positions.MIDDLE,
	timeout: 3000,
	transition: transitions.FADE,
}

const Root = () => (
	<AlertProvider template={AlertTemplate} {...options}>
		<App />
	</AlertProvider>
)

ReactDOM.render(<Root />, document.getElementById('root'))

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.register()
