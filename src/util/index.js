/** funcao para validar URL */
function isValidURL(str) {
	const pattern = new RegExp(
		'^(https?:\\/\\/)?' + // protocol
		'((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
		'((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
		'(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
		'(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
			'(\\#[-a-z\\d_]*)?$',
		'i',
	)
	return !!pattern.test(str)
}

/** funcao para validar email */
function isvalidEmail(str) {
	const emailReg = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/
	return emailReg.test(str)
}

/** funcao para validar CPF */
function isValidCPF(str) {
	let soma
	let resto
	soma = 0
	if (str == '00000000000') return false

	for (let i = 1; i <= 9; i++) soma = soma + parseInt(str.substring(i - 1, i)) * (11 - i)
	resto = (soma * 10) % 11

	if (resto == 10 || resto == 11) resto = 0
	if (resto != parseInt(str.substring(9, 10))) return false

	soma = 0
	for (let i = 1; i <= 10; i++) soma = soma + parseInt(str.substring(i - 1, i)) * (12 - i)
	resto = (soma * 10) % 11

	if (resto == 10 || resto == 11) resto = 0
	if (resto != parseInt(str.substring(10, 11))) return false
	return true
}

export { isValidURL, isValidCPF }
