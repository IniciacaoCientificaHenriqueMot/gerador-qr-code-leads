import FirebaseService from './firebase-service'

export default class QRCodeService {
	static saveQRCode(data, callback) {
		FirebaseService.addData('code', data, callback)
	}
	static getQRCode(term = null, callback) {
		FirebaseService.getData('code', callback, 'tipo')
	}
	static deleteQRCode(key, callback) {
		FirebaseService.deleteData('code', key, callback)
	}
}
