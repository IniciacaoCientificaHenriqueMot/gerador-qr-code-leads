import React from 'react'
import { Container, Row, Col } from 'react-bootstrap'
import { QRCode } from '../components'
import ReactToPrint from 'react-to-print'

export default function QRCodePreview(props) {
	const componentRef = React.useRef()

	return (
		<>
			<Container>
				<Row>
					<Col>
						<ReactToPrint
							trigger={() => <button className='btn btn-secondary'>imprimir</button>}
							content={() => componentRef.current}
							className='float-right'
						/>
					</Col>
				</Row>
				<Row>
					<Col>
						<div ref={componentRef}>{/* <QRCode data={props.data} /> */}</div>
					</Col>
				</Row>
			</Container>
		</>
	)
}
