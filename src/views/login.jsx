import React from 'react'
import Loader from 'react-loader-spinner'
import { FirebaseService } from '../services'
import { useAlert } from 'react-alert'

export default function Login(props) {
	const [user] = React.useState({ email: null, password: null })
	const [loading, setLoading] = React.useState(false)
	const alert = useAlert()

	return (
		<form className='form-signin'>
			<div className='form-label-group'>
				<label htmlFor='email'>E-mail</label>
				<input
					type='email'
					id='email'
					className='form-control'
					placeholder='E-mail'
					onChange={e => (user.email = e.target.value)}
				/>
			</div>

			<div className='form-label-group'>
				<label htmlFor='senha'>Senha</label>
				<input
					type='password'
					id='senha'
					className='form-control'
					placeholder='Senha'
					onChange={e => (user.password = e.target.value)}
				/>
			</div>

			<button
				className='btn btn-lg btn-primary btn-block'
				type='button'
				onClick={() => {
					setLoading(true)
					FirebaseService.login(user, e => {
						if (e.code && e.code === 'auth/user-not-found') alert.error(`E-mail não localizado ou senha inválida.`)
						else if (e.code) alert.error(e.message)
						setLoading(false)
					})
				}}
				style={{ marginTop: 5 }}>
				{loading ? <Loader type='ThreeDots' color='#00BFFF' height={30} width={30} /> : 'Login'}
			</button>
			<p className='mt-5 mb-3 text-muted text-center'>&copy; LABCTI - LID - 2019</p>
		</form>
	)
}
