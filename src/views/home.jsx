import React from 'react'
import { Nav, Row, Col } from 'react-bootstrap'
import { firebaseAuth } from '../config'

export default function Home(props) {
	return (
		<>
			<Row>
				<Col md={{ span: 6, offset: 3 }}>
					<Nav.Link href='/qrcode/generate' className='btn btn-primary btn-default btn-menu'>
						Gerar QRCode
					</Nav.Link>
					<Nav.Link href='/qrcode' className='btn btn-primary btn-default btn-menu'>
						Consultar QRCode gerado
					</Nav.Link>
					<button
						className='btn btn-block btn-primary btn-info btn-menu'
						onClick={() => {
							firebaseAuth.signOut()
							localStorage.setItem('@leadapp-usr', null)
						}}>
						Logout
					</button>
				</Col>
			</Row>
		</>
	)
}
