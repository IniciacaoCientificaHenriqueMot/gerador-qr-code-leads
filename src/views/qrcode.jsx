import React from 'react'
import { Table, Card, Row, Col } from 'react-bootstrap'
import { Barra, ModalQRCode, Loading } from '../components'
import { QRCodeService } from '../services'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faQrcode, faTrash } from '@fortawesome/free-solid-svg-icons'

export default function QRCode(props) {
	const initial = { tipo: 'externo', url: '', title: '', descricao: '', file: null }
	const [data, setData] = React.useState(null)
	const [show, setShow] = React.useState(false)
	const [form, setForm] = React.useState(initial)

	React.useEffect(() => {
		QRCodeService.getQRCode(null, data => setData(data))
	}, [])

	return (
		<>
			<Row>
				<Col>
					<Card>
						<Barra buttons={[{ name: 'home', url: '/' }]} />
						<Card.Body>
							<Table striped bordered responsive hover size='sm'>
								<thead>
									<tr>
										<th>Tipo</th>
										<th>Título</th>
										<th>Descrição</th>
										<th>&nbsp;</th>
									</tr>
								</thead>
								<tbody>
									{!data && (
										<tr>
											<td colSpan={100}>
												<Loading />
											</td>
										</tr>
									)}
									{data && data.length === 0 && (
										<tr>
											<td colSpan={100}>sem registros encontrados...</td>
										</tr>
									)}
									{data &&
										data.length > 0 &&
										data.map((e, i) => (
											<tr key={i}>
												<td>{e.tipo}</td>
												<td>{e.title}</td>
												<td>{e.descricao}</td>
												<td style={{}}>
													<div>
														<center>
															<button
																className='btn btn-info margin-right'
																onClick={() => {
																	setForm({ tipo: e.tipo, title: e.title, url: e.url, descricao: e.descricao })
																	setShow(!show)
																}}>
																<FontAwesomeIcon icon={faQrcode} />
															</button>
															<button
																className='btn btn-danger'
																onClick={() => {
																	QRCodeService.deleteQRCode(e.id, e => console.log(e))
																}}>
																<FontAwesomeIcon icon={faTrash} />
															</button>
														</center>
													</div>
												</td>
											</tr>
										))}
								</tbody>
							</Table>
						</Card.Body>
					</Card>
					<ModalQRCode
						data={form}
						show={show}
						onHide={() => {
							setShow(false)
							setForm(initial)
						}}
					/>
				</Col>
			</Row>
		</>
	)
}
