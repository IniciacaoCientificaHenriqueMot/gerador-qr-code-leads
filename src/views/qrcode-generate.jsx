import React from 'react'
import { Card, Button, Form, Row, Col } from 'react-bootstrap'
import { useAlert } from 'react-alert'
import { Barra, ModalQRCode } from '../components'
import { QRCodeService } from '../services'
import { isValidURL } from '../util'

export default function QRCodeGenerate(props) {
	const alert = useAlert()
	const initial = { tipo: 'externo', title: '', url: '', descricao: '', file: null }
	const [form, setForm] = React.useState(initial)
	const [error, setError] = React.useState({})
	const [show, setShow] = React.useState(false)

	function handleChange(e) {
		setForm({ ...form, [e.target.id]: e.target.value })
	}

	function handleSubmit(event) {
		event.preventDefault()
		const error = validate()
		if (Object.values(error).length > 0) alert.error('Erros foram encontrados no seu formulário.')
		else {
			QRCodeService.saveQRCode(form, e => console.log(e))
			setShow(!show)
		}
	}

	function validate() {
		const errors = {}
		if (form.title.trim().length === 0) errors.title = 'O campo Título é requerido.'
		else if (form.title.trim().length <= 3) errors.title = 'O campo Título parece não estar correto.'
		if (form.url.trim().length === 0) errors.url = 'O campo URL é requerido.'
		// else if (isValidURL(form.url.trim())) errors.url = 'O campo URL parece não ser uma URL válida.'
		setError(errors)
		return errors
	}

	return (
		<>
			<Row>
				<Col md={{ span: 8, offset: 2 }}>
					<Form onSubmit={e => handleSubmit(e)} noValidate>
						<Card>
							<Barra buttons={[{ name: 'home', url: '/' }]} />
							<Card.Body>
								{/* 
						<Row>
							<Col>
								<Form.Group>
									<Form.Label>Tipo de link</Form.Label>
									<Form.Check
										name='tipo'
										type='radio'
										value='externo'
										label='externo'
										id='tipo'
										checked={form.tipo === 'externo'}
										onChange={e => handleChange(e)}
									/>
								</Form.Group>
							</Col>
							<Col>
								<Form.Group>
									<Form.Label>&nbsp;</Form.Label>
									<Form.Check
										name='tipo'
										type='radio'
										value='interno'
										label='interno'
										id='tipo'
										checked={form.tipo === 'interno'}
										onChange={e => handleChange(e)}
									/>
								</Form.Group>
							</Col>
						</Row> 
						*/}
								<Row>
									<Col>
										<Form.Group>
											<Form.Label>Título</Form.Label>
											<Form.Control
												type='text'
												placeholder='informe a título do QRCode'
												id='title'
												value={form.title}
												onChange={e => handleChange(e)}
											/>
											{error.title && <div className='error'>{error.title}</div>}
										</Form.Group>
									</Col>
								</Row>
								{form.tipo === 'externo' && (
									<Row>
										<Col>
											<Form.Group>
												<Form.Label>URL</Form.Label>
												<Form.Control
													type='url'
													placeholder='informe a URL'
													id='url'
													value={form.url}
													onChange={e => handleChange(e)}
												/>
												{error.url && <div className='error'>{error.url}</div>}
											</Form.Group>
										</Col>
									</Row>
								)}
								{form.tipo === 'interno' && (
									<Row>
										<Col>
											<Form.Group>
												<Form.Label>Upload de arquivo</Form.Label>
												<Form.Control
													type='file'
													placeholder='informe o arquivo'
													id='file'
													value={form.file}
													onChange={e => handleChange(e)}
												/>
												{error.url && <div className='error'>{error.url}</div>}
											</Form.Group>
										</Col>
									</Row>
								)}
								<Row>
									<Col>
										<Form.Group>
											<Form.Label>Descrição</Form.Label>
											<Form.Control
												as='textarea'
												rows='5'
												id='descricao'
												value={form.descricao}
												onChange={e => handleChange(e)}
											/>
											{error.descricao && <div className='error'>{error.descricao}</div>}
										</Form.Group>
									</Col>
								</Row>
								<Button type='submit' className='float-right'>
									Gerar QRCode
								</Button>
							</Card.Body>
						</Card>
					</Form>
					<ModalQRCode
						data={form}
						show={show}
						onHide={() => {
							setShow(false)
							setForm(initial)
						}}
					/>
				</Col>
			</Row>
		</>
	)
}
