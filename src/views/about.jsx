import React from 'react'
import logo from '../assets/logo-estacio.png'

export default function About(props) {
	return (
		<>
			<center>
				<img src={logo} alt='logo LID' style={{ alignSelf: 'center' }} />
				<h2>Gerador de QRCode Estácio</h2>
				<p>Laboratório de Inovação e Desenvolvimento</p>
			</center>
		</>
	)
}
