import React, { Suspense, lazy } from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import { Container, Row, Col } from 'react-bootstrap'
import { firebaseAuth, localuser } from '../config'
import { Loading } from '../components'

import logo from '../assets/logo-estacio.png'

/** importacao das views */
const Login = lazy(() => import('../views/login'))
const Generate = lazy(() => import('../views/qrcode-generate'))
const QRCode = lazy(() => import('../views/qrcode'))
const Preview = lazy(() => import('../views/qrcode-preview'))
const Home = lazy(() => import('../views/home'))
const About = lazy(() => import('../views/about'))

export default function App() {
	const [loggeduser, setLoggeduser] = React.useState(null)
	const usr = JSON.parse(localStorage.getItem(localuser))

	React.useEffect(() => {
		firebaseAuth.onAuthStateChanged(user => {
			if (user) {
				localStorage.setItem(localuser, JSON.stringify(user))
				setLoggeduser(user)
			} else {
				localStorage.setItem(localuser, null)
				setLoggeduser(null)
			}
		})
	}, [usr])

	return usr ? <Logged usr={usr} /> : <NotLogged />
}

function NotLogged() {
	return (
		<>
			<div className='header'>
				<center>
					<img src={logo} alt='' />
					<h2>Gerador de QRCode</h2>
				</center>
			</div>
			<Container>
				<Row>
					<Col md={{ span: 6, offset: 3 }}>
						<Router>
							<Suspense fallback={<Loading />}>
								<Switch>
									<Route path='/' component={Login} />
								</Switch>
							</Suspense>
						</Router>
					</Col>
				</Row>
			</Container>
		</>
	)
}

function Logged(props) {
	const { usr } = props
	return (
		<>
			<div className='header'>
				<center>
					<img src={logo} alt='' />
					<h2>Gerador de QRCode</h2>
					<p>usuário: {usr.email}</p>
				</center>
			</div>
			<Container fluid>
				<Router>
					<Suspense fallback={<Loading />}>
						<Switch>
							<Route exact path='/qrcode/generate' component={Generate} />
							<Route exact path='/qrcode/preview' component={Preview} />
							<Route exact path='/qrcode' component={QRCode} />
							<Route exact path='/about' component={About} />
							<Route path='/' component={Home} />
						</Switch>
					</Suspense>
				</Router>
				<p className='mt-4 mb-3 text-muted text-center'>&copy; LABCTI - LID - 2019</p>
			</Container>
		</>
	)
}
